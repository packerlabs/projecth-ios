//
//  Milestone.h
//  ProjectH
//
//  Created by My Star on 10/13/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface StepModel : JSONModel
@property NSString *_id; //": "59a2d0e24cb0e1511e415309",
@property NSString *displayTitle; //": "Milestone Test",
@property NSString *description; //": "<p>This is the first MIlestone!</p><p><br/></p>",
@property NSString *milestoneID;  //59a2d0e24cb0e1511e415309",
@property NSString * status; //": "1",
@property int order; //": 1,
@property NSString *createdOn; //": "2017-08-27T14:02:10.049Z",
@property NSString *lastUpdatedOn;//": "2017-08-27T14:02:10.049Z",
@property int __v; //": 0,
@property BOOL isArchived; //": false,
@property BOOL containsMedia; //": false,
@property BOOL requireReview; //": false,
@property BOOL completed; //": false,
@property BOOL canUserComplete; //": false
@end

//
//  Milestone.h
//  ProjectH
//
//  Created by My Star on 10/13/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface MyProgressModel : JSONModel
@property NSString *milestoneID;    //": "59a2d0e24cb0e1511e415309",
@property NSString *displayTitle;   //": "Milestone Test",
@property NSString *description;    //": "<p>This is the first MIlestone!</p><p><br/></p>",
@property NSArray *stepsRequired;
@property NSArray *stepsPending;
@property NSArray *stepsCompleted;

@end

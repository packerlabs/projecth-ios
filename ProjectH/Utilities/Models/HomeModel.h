//
//  Milestone.h
//  ProjectH
//
//  Created by My Star on 10/13/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface HomeModel : JSONModel
@property NSString *_id; //": "59a2d0e24cb0e1511e415309",
@property NSString *displayTitle; //": "Milestone Test",
@property NSString *description; //": "<p>This is the first MIlestone!</p><p><br/></p>",
@property NSString * status; //": "1",
@property int order; //": 1,
@property NSString *createdOn; //": "2017-08-27T14:02:10.049Z",
@property NSString *lastUpdatedOn;//": "2017-08-27T14:02:10.049Z",
@property NSString *organizationID; //": "599b40a7e1a33d50e4b6d4ed",
@property int __v; //": 0,
@property BOOL isArchived; //": false,
@property NSArray *steps;
@end

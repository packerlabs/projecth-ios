//
//  CurrentUser.m
//  RT66 Passport
//
//  Created by My Star on 8/7/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "CurrentUser.h"

@implementation CurrentUser
+ (CurrentUser *)instance
{
    static CurrentUser *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CurrentUser alloc] init];
    });
    return sharedInstance;
}
- (void)saveUser:(NSDictionary *)response{
    GLOBALINS.userId = response[USER_ID];
    GLOBALINS.token = response[USER_TOKEN];
    NSMutableDictionary *dic = [NSMutableDictionary new];
    dic[USER_ID] = GLOBALINS.userId;
    dic[USER_TOKEN] = GLOBALINS.token;
    dic[USER_PASSWORD] = GLOBALINS.password;
    [UserDefaults setObject:dic forKey:@"CurrentUser"];
}
- (BOOL)readUser{
    NSDictionary *dic = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CurrentUser"];
    GLOBALINS.userId = dic[USER_ID];
    GLOBALINS.token = dic[USER_TOKEN];
    GLOBALINS.password = dic[USER_PASSWORD];
    
    NSLog(@"USER ID ===>%@",GLOBALINS.userId);
    NSLog(@"USER TOKEN ===>%@",GLOBALINS.token);
    NSLog(@"USER PASSWORD ===>%@",GLOBALINS.password);
    if (GLOBALINS.userId.length && GLOBALINS.token.length) {
        return YES;
    }
    return NO;
}
-(void)loginWithEmail:(NSString *)email password:(NSString *)password Completion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"email"] = email;
    params[@"password"] = password;
    GLOBALINS.password = password;
    [APIManager apiPostURLJson:API_LOGIN_PO uploadParam:(NSDictionary *)params withCompletion:^(NSDictionary *response) {
        NSDictionary *response_ = response[@"data"];
        [self saveUser:response_];
        completion(response_);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
-(void)signUpWithCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure{
    NSString *url = [NSString stringWithFormat:@"%@user/%@",API_HOST,GLOBALINS.userId];
    [APIManager apiGetURL:url parameter:nil withCompletion:^(NSDictionary *response) {
        completion(response);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
-(void)getUserFullInfoWithCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure{
    NSString *url = [NSString stringWithFormat:@"%@user/%@",API_HOST,GLOBALINS.userId];
    [APIManager apiGetURL:url parameter:nil withCompletion:^(NSDictionary *response) {
        completion(response);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
-(void)getUserProgressWithCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure{
    NSString *url = [NSString stringWithFormat:@"%@/%@",API_USERPROGRESS, GLOBALINS.userId];
    [APIManager apiGetURL:url parameter:nil withCompletion:^(NSDictionary *response) {
        [self parseMilestonsWithDictionary:response];
        completion(response);        
    } failure:^(NSError *error) {
        failure(error);
    }];
}
-(void)parseMilestonsWithDictionary :(NSDictionary *)response{
    NSArray *array = response[@"data"];
    NSError *error;
    NSInteger totalStepCount = 0;
    NSInteger completedStepCount = 0;
    GLOBALINS.progress = 0.0f;
    GLOBALINS.myMilestones = [NSMutableArray new];
    for (int i = 0; i<array.count; i++) {
        MyProgressModel *myProgress = [[MyProgressModel alloc] initWithDictionary:array[i] error:&error];
        [GLOBALINS.myMilestones addObject:myProgress];
        totalStepCount += myProgress.stepsRequired.count;
        completedStepCount += myProgress.stepsCompleted.count;
    }
    if (array.count) {
        GLOBALINS.progress = (float)((float)completedStepCount/(float)totalStepCount);
    }
}
@end

//
//  CurrentUser.h
//  RT66 Passport
//
//  Created by My Star on 8/7/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "utilities.h"
#import <JSONModel/JSONModel.h>
@interface CurrentUser : JSONModel

@property NSString *_id;//
@property NSString *fullName;//
@property NSString *email;//
@property NSString *password;
@property NSString *phone;//
@property NSString *preferenceOfContact;
@property NSString *allowsText;
@property NSString *education;
@property NSString *employer;
@property NSString *annualSalary;
@property NSString *banker;
@property NSString *typeOfLoan;
@property NSString *address;
@property NSString *birthDate;
@property NSString *organizationID;//
//
@property NSString *createdOn;
@property NSInteger __v;
//@property NSString *realtorID;
@property BOOL isArchived;
@property NSArray *stepsPending;
@property NSArray *stepProgress;
@property NSArray *mileStoneProgress;

//"_id": "59a2d1724cb0e1511e41530e",
//"fullName": "Test Account",
//"email": "team@packerlabs.com",
//"password": "$2a$10$HmIZGbWrd2QZZcC8QkGRt.1LuAoXpcNye58SW5dHVAWp39uQ4hWem",
//"preferenceOfContact": "phone",
//"allowsText": "true",
//"organizationID": "599b40a7e1a33d50e4b6d4ed",
//"createdOn": "2017-08-27T14:04:34.924Z",
//"__v": 0,
//"realtorID": null,
//"isArchived": false,
//"stepsPending": [],
//"stepProgress": [
//                 "59a2d10e4cb0e1511e41530b",
//                 "59a2d11d4cb0e1511e41530c",
//                 "59a2d1394cb0e1511e41530d",
//                 "59df9c1510d62f1aeeea47a5",
//                 "59e0f21b10d62f1aeeea47a7"
//                 ],
//"mileStoneProgress": [
//                      "59a2d0e24cb0e1511e415309",
//                      "59a2d0f54cb0e1511e41530a",
//                      "59df832b10d62f1aeeea47a4",
//                      "59df9ebb10d62f1aeeea47a6"
//                      ],
//"typeOfLoan": "Personal",
//"banker": "Personal",
//"annualSalary": "1000000",
//"employer": "Self",
//"birthDate": "2017-08-27T14:03:48.273Z",
//"education": "Bachelor's",
//"phone": "6182077553",
//"address": "1000 S Brehm Ln, Carbondale, IL 62901"

+(CurrentUser *)instance;
-(void)loginWithEmail:(NSString *)email password:(NSString *)password Completion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure;
-(void)getUserFullInfoWithCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure;
-(void)getUserProgressWithCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure;
-(void)saveUser:(NSDictionary *)dic;
-(BOOL)readUser;

@end

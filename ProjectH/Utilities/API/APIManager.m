//
//  API.m
//  ProjectH
//
//  Created by My Star on 10/7/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "APIManager.h"

@implementation APIManager
+ (APIManager *)instance
{
    static APIManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[APIManager alloc] init];
    });
    return sharedInstance;
}
+(void)apiGetURL:(NSString *)url parameter:(NSDictionary *)parameters withCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSLog(@"%@",GLOBALINS.token);
    [manager.requestSerializer setValue:GLOBALINS.token forHTTPHeaderField:@"Authorization"];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if ([APIINS responseErrorCheck:responseObject]) {
            completion(responseObject);
        }else{
            NSError *err = [[NSError alloc] init];
            failure(err);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(error);
    }];
}
+ (void)apiPostURL:(NSString *)url uploadParam:(NSDictionary *)uploadDic withCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure
{
   
    NSMutableURLRequest *request;
    request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:uploadDic constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {

    } error:nil];
    NSLog(@"Request = %@",request);
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];



    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{

                      });
                  }
                  completionHandler:^(NSURLResponse  *response, id  responseObject, NSError  *error) {
                      if (error) {
                          NSLog(@"Error: %@, %@, %@", error, response, responseObject);
                          failure(error);
                      }else{
                          if ([APIINS responseErrorCheck:responseObject]) {
                              completion(responseObject);
                          }else{
                              NSError *err = [[NSError alloc] init];
                              failure(err);
                          }
                      }
                  }];
    [uploadTask resume];

}
+ (void)apiPostURLJson:(NSString *)url uploadParam:(NSDictionary *)uploadDic withCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:uploadDic options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req;
    req = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:uploadDic constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {

    } error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    if (GLOBALINS.token.length) {
        [req setValue:GLOBALINS.token forHTTPHeaderField:@"Authorization"];
    }
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                completion(responseObject);
            }
        } else {
            failure(error);
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
        }
    }] resume];
}

+ (void)apiPutURL:(NSString *)url uploadParam:(NSDictionary *)uploadDic withCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:uploadDic options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:GLOBALINS.token forHTTPHeaderField:@"Authorization"];
    [manager PUT:url parameters:uploadDic success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([APIINS responseErrorCheck:responseObject]) {
            completion(responseObject);
        }else{
            NSError *err = [[NSError alloc] init];
            failure(err);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
  
        failure(error);
    }];
}
- (bool)responseErrorCheck:(NSDictionary*)response
{
    if (response) {
        if (!response.count) {
            return NO;
        }else{
            for (id obj in response.allValues) {
                if ([obj isKindOfClass:[NSNull class]]) {
                    return NO;
                }
            }
            return YES;
        }
    }else{
        return NO;
    }
}
@end

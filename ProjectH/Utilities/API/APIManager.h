//
//  API.h
//  ProjectH
//
//  Created by My Star on 10/7/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "utilities.h"
@interface APIManager : NSObject
+ (APIManager *)instance;
+(void)apiGetURL:(NSString *)url parameter:(NSDictionary *)parameters  withCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure;
+ (void)apiPostURL:(NSString *)url uploadParam:(NSDictionary *)uploadDic withCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure;
+ (void)apiPostURLJson:(NSString *)url uploadParam:(NSDictionary *)uploadDic withCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure;
+ (void)apiPutURL:(NSString *)url uploadParam:(NSDictionary *)uploadDic withCompletion:(void(^)(NSDictionary *))completion failure:(void (^)(NSError *))failure;

- (bool)responseErrorCheck:(NSDictionary*)response;
@end

//
// Copyright (c) 2016 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "utilities.h"
//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface Location : NSObject <CLLocationManagerDelegate>
//-------------------------------------------------------------------------------------------------------------------------------------------------

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *strLocation;

+ (Location *)shared;

+ (void)start;
+ (void)stop;

+ (CLLocationDegrees)latitude;
+ (CLLocationDegrees)longitude;
+ (NSString *)address;
+ (NSString *)gpsLocation;
-(void)getAddressFromPosition :(CLLocation *)location :(void (^)(LMAddress *))completion failure:(void (^)(NSError *))failure;
-(void)setMyAddressAndLocation:(LMAddress*)address;
+(void)getActualAdress :(NSString *)address completion:(void (^)(LMAddress *))completion failure:(void (^)(NSError *))failure;
@end


#pragma mark - DEVICE INFO
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define Fontcell (IS_IPAD ? 70 : 45)

#define        SCREEN_WIDTH                         [UIScreen mainScreen].bounds.size.width
#define        SCREEN_HEIGHT                        [UIScreen mainScreen].bounds.size.height
//------------------------------------------------------------------------------------------------------
#pragma mark - SYSTEM_VERSION

#define        SYSTEM_VERSION                                [[UIDevice currentDevice] systemVersion]
#define        SYSTEM_VERSION_EQUAL_TO(v)                    ([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedSame)
#define        SYSTEM_VERSION_GREATER_THAN(v)                ([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedDescending)
#define        SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)    ([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedAscending)
#define        SYSTEM_VERSION_LESS_THAN(v)                   ([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedAscending)
#define        SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)       ([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedDescending)
//-----------------------------------------------------------------------------------

#pragma mark - COLORs

#define		HEXCOLOR(c) [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]
#define		COLOR_BACKGROUND                    HEXCOLOR(0x22A4DAFF)
#define		COLOR_NAVIGATIONBAR                 HEXCOLOR(0x27547DFF)
#define		COLOR_NAVIGATIONBARTEXT             HEXCOLOR(0xfbffe2FF)
#define     COLOR_NAV_BAR                       [UIColor colorWithRed:(41.0/255.0) green:(171.0/255) blue:(226.0/255) alpha:1.0]
#define     COLOR_HOME_BG                       [UIColor colorWithRed:(235.0/255.0) green:(235.0/255) blue:(235.0/255) alpha:1.0]
#define     COLOR_NAV_BLUE                      [UIColor colorWithRed:(0.0/255.0) green:(120.0/255) blue:(255.0/255) alpha:1.0]
#define     COLOR_NAV_BLUE                      [UIColor colorWithRed:(0.0/255.0) green:(120.0/255) blue:(255.0/255) alpha:1.0]
#define     COLOR_NAV_WHITE                     [UIColor colorWithRed:(255.0/255.0) green:(255.0/255) blue:(255.0/255) alpha:1.0]
#define     COLOR_SLIDER_BG                     [UIColor colorWithRed:(41.0/255.0) green:(41.0/255) blue:(41.0/255) alpha:1.0]
#define     COLOR_BTN_BACKGROUND                [UIColor colorWithRed:126/225.0 green:201/255.0 blue:55/255.0 alpha:1.0]

//---------------------------------------------------------------------------------
#pragma mark - APP KEYs
#define     ONESIGNAL_APPID                     @"dfa23d25-0358-4c9f-9f69-352065815bf8"
#define     GOOGLEMAPKEY                        @"AIzaSyDp2d1vj_Rqi8lrbqgUHCcyTdQ-HWJIkQY"

//---------------------------------------------------------------------------------
#pragma mark - CURRENTUSER

#define        USER_IMAGE                               @"userImg"              //  String*
#define        USER_ID                                  @"userId"               //  String*
#define        USER_FULLNAME                            @"fullName"             //  String*
#define		   USER_EMAIL			                    @"email"				//  String*
#define        USER_PASSWORD                            @"password"             //  String*
#define        USER_PHONE                               @"phone"                //  String*
#define        USER_PREFERENCEOFCONTACT                 @"preferenceOfContact"  //  String*
#define        USER_ALLOWTEXT                           @"allowsText"           //  String*
#define        USER_EDUCATION                           @"education"            //  String*
#define        USER_BIRTHDATE                           @"birthDate"            //  String*
#define        USER_EMPLOYER                            @"employer"             //  String*
#define        USER_SALARY                              @"annualSalary"         //  String*
#define        USER_BANKER                              @"banker"               //  String*
#define        USER_LOAN                                @"typeOfLoan"           //  String*
#define        USER_ORGANIZATION                        @"organization"         //  String*
#define        USER_ORGANIZATIONID                      @"organizationID"       //  String*
#define        USER_TOKEN                               @"token"                //  String*
#define        USER_ADDRESS                             @"address"              //  String*

//---------------------------------------------------------------------------------

#pragma mark - NOTIFICATION KEYs

#define		NOTI_APP_STARTED			@"NotificationAppStarted"
#define		NOTI_USER_LOGGED_IN			@"NotificationUserLoggedIn"
#define		NOTI_USER_LOGGED_OUT		@"NotificationUserLoggedOut"
#define		NOTI_MEONLINE     			@"meonline"
#define		NOTI_MEOFFLINE     			@"meoffline"
#define		NOTI_APP_TIMERESET			@"ApptimeReset"
#define     NOTI_QUESTIONCHANGE         @"questionchange"
#define     NOTI_CHANGEPROGRESS         @"changeprogress"
#define		NOTIFI_COMPLICATED          @"notificationcomplicated"
#define     NOTI_SLIDEIMAGEINDEX        @"imageIdex"


#define     CHANGEVC                            @"CHANGEVC"
#define     GOHOMELISTVC                        @"gohomelistvc"
#define     GOMYFAVORITESVC                     @"gomyfavoritesvc"
#define     GOMILESTONEVC                       @"gomilestonevc"
#define     GOPROGRESSVC                        @"goprogressvc"
#define     GOABOUTVC                           @"goaboutvc"
#define     GOCONTACTVC                         @"gocontactvc"
#define     GOSUPPORTVC                         @"gosupportvc"
#define     GOEDITPROFILE                       @"goeditprofilevc"
//-----------------------------------------------------------------------------------

#pragma mark - ERRORs
#define ERROR_NETWORK                           @"Sorry, no internet connection"
//-----------------------------------------------------------------------------------
#pragma mark - INSTANCE

#define GLOBALINS                               ((Global *)[Global instance])
#define USERINS                                 ((CurrentUser *)[CurrentUser instance])
#define APIINS                                  ((APIManager *)[APIManager instance])
//-----------------------------------------------------------------------------------
#pragma mark - APP DEFAULT VALUEs
#define UNKNOWNUSERIMG                          [UIIM]
#define		VIDEO_LENGTH						5
#define		PASSWORD_LENGTH						6
#define		DOWNLOAD_TIMEOUT					300

#define		STATUS_LOADING						1
#define		STATUS_SUCCEED						2
#define		STATUS_MANUAL						3

#define		NETWORK_MANUAL						1
#define		NETWORK_WIFI						2
#define		NETWORK_ALL							3

#define		DEL_ACCOUNT_NONE					1
#define		DEL_ACCOUNT_ONE						2

#define     MAPTYPE_ALLHOMES                    1
#define     MAPTYPE_MYFAVORITES                 2
#define     MAPTYPE_DETAIL                      3

#define     MAPZOOM_ALLHOMES                    14
#define     MAPZOOM_MYFAVORITES                 14
#define     MAPZOOM_DETAIL                      19
//-----------------------------------------------------------------------------------
#pragma mark - APIs
#define     API_HOST                @"http://52.23.156.36:3000/api/"
#define     API_SIGNUP_PO           [NSString stringWithFormat:@"%@%@",API_HOST,@"user"]
#define     API_LOGIN_PO            [NSString stringWithFormat:@"%@%@",API_HOST,@"user/login"]
#define     API_ORGANIZATION_       [NSString stringWithFormat:@"%@%@",API_HOST,@"organization"]
#define     API_MILESTONES          [NSString stringWithFormat:@"%@%@",API_HOST,@"milestones"]
#define     API_STEPS_ID            [NSString stringWithFormat:@"%@%@",API_HOST,@"stepId"]
#define     API_STEPLIST_G          [NSString stringWithFormat:@"%@%@",API_HOST,@"stepsList"]
#define     API_USERPROGRESS        [NSString stringWithFormat:@"%@%@",API_HOST,@"userProgress"]
#define     API_STEPS_PU            [NSString stringWithFormat:@"%@%@",API_HOST,@"user"]
#define     API_MARKSTEPS_PO        [NSString stringWithFormat:@"%@%@",API_HOST,@"user/markStepAsCompleted"]
#define     API_EDITEPROFILE_PU     [NSString stringWithFormat:@"%@%@",API_HOST,@"user"]

#define     ARRAY_ALLOWTEXT         [NSMutableArray arrayWithObjects:@"Yes",@"No", nil]
#define     ARRAY_EDUCATION         [NSMutableArray arrayWithObjects:@"Some High School",@"High School Completed",@"GED",@"Bachelor's",@"Other", nil]
#define     ARRAY_CONTACT           [NSMutableArray arrayWithObjects:@"Phone",@"Email", nil]

//http://52.23.161.71:3000/#/companies

// jpacker@siu.edu | A5kgodwhy.

// dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
//
// });

//dispatch_async(dispatch_get_main_queue(), ^{
//
//});

//dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//    dispatch_async(dispatch_get_main_queue(), ^{
//    });
//});

//dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
//dispatch_after(time, dispatch_get_main_queue(), ^(void){
//
//});

//static dispatch_once_t once;
//dispatch_once(&once, ^{
//});

//[self performSelector:@selector(delayedReload) withObject:nil afterDelay:0.25];

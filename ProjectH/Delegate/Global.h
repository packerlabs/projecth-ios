//
//  Global.h
//  PWFM
//
//  Created by My Star on 5/8/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "utilities.h"
#import "MyProgressModel.h"
#import "HomeModel.h"
@interface Global : NSObject
#pragma mark - UIs
@property UIViewController *mainVC;
@property UIView *pageContainer;
@property UIViewController *currentloadedViewController;
@property CGFloat naviHeight;
#pragma mark - APP FLAGs/ INDEXs
@property BOOL isAppStart;
@property BOOL isMyProgress;
@property NSInteger mayType;
@property NSInteger selectedStepIndex;
@property NSInteger selectedQuestionViewIndex;

#pragma mark - Global Parameter
@property NSMutableArray *milestones;
@property NSMutableArray *steps;
@property NSMutableArray *myMilestones;
@property MyProgressModel *progressModel;
@property NSMutableArray *homeModels;
@property float progress;

#pragma mark - USER INFO
@property NSString *userId;
@property NSString *token;
@property NSString *password;
@property UIImage *userImg;



#pragma mark - CLASS METHODs
+(Global *)instance;
+(void)roundBorderSet:(id)senser borderColor:(UIColor *)color;
+(void)roundCornerSet:(id)senser;
+(BOOL) validEmail:(NSString *) strEmail;
+(void)confirmMessage :(UIViewController*)taget  :(NSString *)title :(NSString *)message;
+(NSString *)getDateFromTimestamp:(long long)timestamp;

#pragma mark - INSTANCE METHODs
-(void)switchVC:(UIViewController *)vc;
+(void)goPushFromNVC :(UIViewController *)fromNVC to:(UIViewController*)toVC;
+(void)goPushFromVC :(UIViewController *)fromVC to:(UIViewController*)toVC;
+(void)gotoPresentFromVC :(UIViewController *)fromVC to:(UIViewController*)toVC;
+(void)popupVCwithFrom: (UIViewController *)fromVC to:(UIViewController *)toVC;
-(void)parameterInit;
@end

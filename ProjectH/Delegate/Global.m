//
//  Global.m
//  PWFM
//
//  Created by My Star on 5/8/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "Global.h"
#import <UIKit/UIKit.h>
@implementation Global

#pragma mark - CLASS METHODs
+ (Global *)instance
{
    static Global *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Global alloc] init];
    });
    return sharedInstance;
}
+(void)roundBorderSet:(id)senser borderColor:(UIColor *)color
{
    [[senser layer] setCornerRadius:5.0f];
    [[senser layer] setMasksToBounds:YES];
    
    [[senser layer] setBorderWidth: 1.f];
    [[senser layer] setBorderColor:color.CGColor];
}
+(void)roundCornerSet:(id)senser
{
    [[senser layer] setCornerRadius:5.0f];
    [[senser layer] setMasksToBounds:YES];
}

+(void)confirmMessage :(UIViewController*)taget  :(NSString *)title :(NSString *)message
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
    
    
    [alertController addAction:cancelAction];
    
    [taget presentViewController:alertController animated:YES completion:nil];
}

+ (BOOL) validEmail:(NSString *) strEmail {
    NSArray * array = [strEmail componentsSeparatedByString:@"@"];
    if (array.count == 2) {
        array = [array[1] componentsSeparatedByString:@"."];
        if (array.count > 1) {
            return YES;
        }
    }
    return NO;
}

+(NSString *)getDateFromTimestamp:(long long)timestamp{
    NSTimeInterval seconds = timestamp / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
    NSLog(@"ans : %@",date);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSLog(@"result: %@", [dateFormatter stringFromDate:date]);
    NSString *dateStr = [dateFormatter stringFromDate:date];
    return dateStr;
}
#pragma mark - INSTANCE METHODs
- (void)parameterInit
{
    self.isAppStart = NO;
    self.isMyProgress = NO;
    self.progress = 0.0f;
    self.mayType = MAPTYPE_ALLHOMES;
    NSData *userImgData = [UserDefaults objectForKey:USER_IMAGE];
    if (userImgData) {
        self.userImg = [UIImage imageWithData:userImgData];
    }
    
}
+(void)goPushFromNVC :(UIViewController *)fromNVC to:(UIViewController*)toVC{
    [fromNVC.navigationController pushViewController:toVC animated:YES];
}
+(void)goPushFromVC :(UIViewController *)fromVC to:(UIViewController*)toVC{
    NavigationController *navController = [[NavigationController alloc] initWithRootViewController:fromVC];
    [navController.navigationController pushViewController:toVC animated:YES];
}
+(void)gotoPresentFromVC :(UIViewController *)fromVC to:(UIViewController*)toVC{
    [fromVC presentViewController:toVC animated:YES completion:nil];
}
+(void)popupVCwithFrom: (UIViewController *)fromVC to:(UIViewController *)toVC{
    [STPopupNavigationBar appearance].barTintColor = [UIColor colorWithRed:0.20 green:0.60 blue:0.86 alpha:1.0];
    [STPopupNavigationBar appearance].tintColor = [UIColor whiteColor];
    [STPopupNavigationBar appearance].barStyle = UIBarStyleDefault;
    [STPopupNavigationBar appearance].titleTextAttributes = @{ NSFontAttributeName: [UIFont systemFontOfSize:17],
                                                               NSForegroundColorAttributeName: [UIColor whiteColor] };
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:toVC];
    popupController.containerView.layer.cornerRadius = 4;
    [popupController presentInViewController:fromVC];
}
+(void)popbottomVCwithFrom: (UIViewController *)fromVC to:(UIViewController *)toVC{
}
+(void)pophorizontalVCwithFrom: (UIViewController *)fromVC to:(UIViewController *)toVC{
}
- (void)switchVC:(UIViewController *)vc{
    
    [self.currentloadedViewController willMoveToParentViewController:nil];
    [self.currentloadedViewController.view removeFromSuperview];
    [self.currentloadedViewController removeFromParentViewController];
    [self.mainVC addChildViewController:vc];
    vc.view.frame = self.pageContainer.bounds;
    vc.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.pageContainer addSubview:vc.view];
    self.currentloadedViewController = vc;
}

@end

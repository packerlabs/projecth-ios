//
//  UserListCell.h
//  ITAGG
//
//  Created by My Star on 5/2/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface HomeListCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *homeImg;

@end


#import "HomeListVC.h"
#import "HomeListCell.h"
#import "UIScrollView+InfiniteScroll.h"
#import "HomeDetailVC.h"

@interface HomeListVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    UIRefreshControl * refreshControl;
    UIRefreshControl * bottomrefreshControl;
    NSString *ingredients;
    int limitLoad;
}

@end

@implementation HomeListVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    refreshControl = [[UIRefreshControl alloc] init];
//    refreshControl.tintColor = [UIColor grayColor];
//    [self.collectionView addSubview:refreshControl];
//    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
//    [self.collectionView addInfiniteScrollWithHandler:^(UICollectionView *collectionView) {
//        [self bottomrefreshTable:^{
//        }];
//    }];
//    [ProgressHUD show:@"Loading..." Interaction:NO];
//    //    [GLOBALINS.multiColorLoader startAnimation];
//
//    ingredients = [self getIngredientKeyfromArray];
//    NSString *url = [NSString stringWithFormat:API_SEARCHFOOD,ingredients,0,10];
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSArray *responseAarray = responseObject[@"hits"];
//        GLOBALINS.recipeModels = [[NSMutableArray alloc]init];
//        if (!responseAarray.count) {
//            [self.view makeToast:@"Sorry, we can't find recipes with ingredients chosen"];
//        }else{
//            NSError *error;
//            for (NSDictionary *dic in responseAarray) {
//                NSDictionary *dic_ = dic[@"recipe"];
//                RecipeModel *recipeModel = [[RecipeModel alloc] initWithDictionary:dic_ error:&error];
//
//                [GLOBALINS.recipeModels addObject:recipeModel];
//
//            }
//            [self.collectionView reloadData];
//        }
////        [GLOBALINS.multiColorLoader stopAnimation];
//        [ProgressHUD dismiss];
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        [ProgressHUD showError:error.description];
//    }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initialize];
    [self setUIs];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)initialize
{
    limitLoad = 10;
    GLOBALINS.mayType = MAPTYPE_ALLHOMES;
}
-(void)setUIs{
    self.navigationItem.title = @"LISTINGS";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(backAction)];

}
#pragma mark - BUTTON ACTIONS
-(void)backAction{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - COLLOECTION VIEW DATASOURCE
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;//GLOBALINS.homeModels.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"HomeListCell";
    HomeListCell *cell = (HomeListCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    HomeListCell *recipeModel = GLOBALINS.recipeModels[indexPath.row];
////    NSLog(@"recipe Image %@",recipeModel.image);
//    [cell.recipImg sd_setImageWithURL:[NSURL URLWithString:recipeModel.image] placeholderImage:[UIImage imageNamed:@"appicon"]];
//    cell.recipeNameL.text = recipeModel.label;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width, height;
    width = (SCREEN_WIDTH-10)/2.0; height = width*0.667;
    return CGSizeMake(width, height);
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}
#pragma mark - COLLOECTION VIEW DELEGATE
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    GLOBALINS.selectedRecipeModel = GLOBALINS.recipeModels[indexPath.row];
    GLOBALINS.mayType = MAPTYPE_DETAIL;
    HomeDetailVC *recipeDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [GLOBALINS.mainVC.navigationController pushViewController:recipeDetailVC animated:YES];
}
#pragma mark - REFRASH COLLECTION VIEW
-(void)fetchRecipeFromservice{
//    NSString *url = [NSString stringWithFormat:API_SEARCHFOOD,ingredients,(int)GLOBALINS.recipeModels.count,(int)GLOBALINS.recipeModels.count+limitLoad];
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSArray *responseAarray = responseObject[@"hits"];
//        if (!responseAarray.count) {
//            [self.view makeToast:@"Sorry, we can't find recipes with ingredients chosen"];
//        }else{
//            if (!GLOBALINS.recipeModels) {
//                GLOBALINS.recipeModels = [[NSMutableArray alloc]init];
//            }
//            NSError *error;
//            for (NSDictionary *dic in responseAarray) {
//                NSDictionary *dic_ = dic[@"recipe"];
//                RecipeModel *recipeModel = [[RecipeModel alloc] initWithDictionary:dic_ error:&error];
//                [GLOBALINS.recipeModels addObject:recipeModel];
//
//            }
//            [self.collectionView reloadData];
//        }
//
//        [self stopLoading];
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        [ProgressHUD showError:error.description];
//    }];
}
//- (void)bottomrefreshTable:(void(^)(void))completion
//{
//    [self fetchRecipeFromservice];
//    if(completion) {
//        completion();
//    }
//}
//-(void) stopLoading
//{
//
//    [bottomrefreshControl endRefreshing];
//    [refreshControl endRefreshing];
//    [self.collectionView finishInfiniteScroll];
//}


-(void)refreshTable{
//    GLOBALINS.recipeModels = [[NSMutableArray alloc] init];
//    [self fetchRecipeFromservice];     
}
//#pragma mark - OTHERs
//-(NSString *)getIngredientKeyfromArray{
//    NSString *choseningredients = @"";
////    for (NSString *ingredient in GLOBALINS.selectIngredients) {
////        NSString *ingredient_ = [ingredient stringByReplacingOccurrencesOfString:@" "
////                                             withString:@"%20"];
////        if (!choseningredients.length) {
////            choseningredients = ingredient_;
////        }else{
////            choseningredients = [NSString stringWithFormat:@"%@%@%@",choseningredients,@"%20",ingredient_];
////        }
////    }
//    return choseningredients;
//}
@end

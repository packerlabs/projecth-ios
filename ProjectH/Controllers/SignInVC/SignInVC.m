
#import "SignInVC.h"
#import "MainVC.h"
@implementation SignInVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.passwordF.secureTextEntry = YES;
}


- (IBAction)signBtn:(id)sender {
    NSString *email, *password;
    email = self.emailF.text;
    password = self.passwordF.text;
    if (![Global validEmail:email]) {
        [ProgressHUD showError:@"Please input valid email."];
        return;
    }
    if (!password.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    [ProgressHUD show:nil Interaction:NO];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"email"] = email;
    params[@"password"] = password;
    [USERINS loginWithEmail:email password:password Completion:^(NSDictionary *response) {
        [self gotoMainScreen];
        [ProgressHUD dismiss];      
    } failure:^(NSError *error) {
        [ProgressHUD showError:error.description];
    }];    
}

#pragma mark TEXTFIELD DELEGETE.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)gotoMainScreen{
    SWRevealViewController *sWRevealViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];

    [self presentViewController:sWRevealViewController animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
@end

//
//  SignInVC.h
//  SIEclipse
//
//  Created by My Star on 7/4/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface SignInVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailF;
@property (weak, nonatomic) IBOutlet UITextField *passwordF;

@end


#import <UIKit/UIKit.h>
#import "utilities.h"
@interface SignUpVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameF;
@property (weak, nonatomic) IBOutlet UITextField *emailF;
@property (weak, nonatomic) IBOutlet UITextField *passwordF;
@property (weak, nonatomic) IBOutlet UITextField *phoneF;
@property (weak, nonatomic) IBOutlet UITextField *birthF;
@property (weak, nonatomic) IBOutlet UITextField *employerF;
@property (weak, nonatomic) IBOutlet UITextField *salaryF;
@property (weak, nonatomic) IBOutlet UITextField *bankerF;
@property (weak, nonatomic) IBOutlet UITextField *loanF;
@property (weak, nonatomic) IBOutlet UITextField *organizationF;
@property (weak, nonatomic) IBOutlet UITextField *addressF;

@property (weak, nonatomic) IBOutlet RKDropDownTextField *allowTextF;
@property (weak, nonatomic) IBOutlet RKDropDownTextField *educationF;
@property (weak, nonatomic) IBOutlet RKDropDownTextField *contactF;
@end


//
//  ViewController.m
//  SIEclipse
//
//  Created by My Star on 7/3/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "SignUpVC.h"
#import "MainVC.h"
@interface SignUpVC ()<RKDropDownTextFieldDelegate, UITextFieldDelegate>{

}

@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUIs];

}

- (void)setUIs{
    self.passwordF.secureTextEntry = YES;
    self.salaryF.keyboardType = UIKeyboardTypeNumberPad;
    self.emailF.keyboardType = UIKeyboardTypeEmailAddress;
    
    self.allowTextF.text = ARRAY_ALLOWTEXT[0];
    self.allowTextF.delegate = self;
    self.allowTextF.values = ARRAY_ALLOWTEXT;
    self.allowTextF.tag = 0;
    
    self.educationF.text = ARRAY_EDUCATION[0];
    self.educationF.delegate = self;
    self.educationF.values = ARRAY_EDUCATION;
    self.educationF.tag = 1;
    
    self.contactF.text = ARRAY_CONTACT[0];
    self.contactF.delegate = self;
    self.contactF.values = ARRAY_CONTACT;
    self.contactF.tag = 2;
}
#pragma mark - BUTTON ACTIONS

- (IBAction)signupBtn:(id)sender
{
    NSString *name, *email, *password, *phone;
    NSString *preferContact, *education, *birthdate, *emploper, *banker, *loan, * organizationid, *address;
    double salary;
    BOOL allowtext;
    name = self.nameF.text;
    email = self.emailF.text;
    password = self.passwordF.text;
    phone = self.phoneF.text;
    preferContact = self.contactF.text;
    allowtext = YES;/*self.allowTextF.text*/;
    education = self.educationF.text;
    birthdate = self.birthF.text;
    emploper = self.employerF.text;
    banker = self.bankerF.text;
    loan = self.loanF.text;
    organizationid = self.organizationF.text;
    salary = [self.salaryF.text doubleValue];
    address = self.addressF.text;
    if (!name.length) {
        [ProgressHUD showError:@"Please input valid name."];
        return;
    }
    if (![Global validEmail:email]) {
        [ProgressHUD showError:@"Please input valid email."];
        return;
    }
    if (!password.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }

    
    if (!phone.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    if (!preferContact.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }

    if (!education.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    if (!birthdate.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    if (!emploper.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    if (!banker.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    if (!loan.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    if (!organizationid.length) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    if (!salary) {
        [ProgressHUD showError:@"Please input valid password."];
        return;
    }
    if (!address.length) {
        [ProgressHUD showError:@"Please input valid address."];
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[USER_FULLNAME] = name;
    params[USER_EMAIL] = email;
    params[USER_PASSWORD] = password;
    params[USER_PHONE] = phone;
    params[USER_PREFERENCEOFCONTACT] = preferContact;
    params[USER_ALLOWTEXT] = @(allowtext);
    params[USER_EDUCATION] = education;
    params[USER_BIRTHDATE] = birthdate;
    params[USER_EMPLOYER] = emploper;
    params[USER_SALARY] = @(salary);
    params[USER_BANKER] = banker;
    params[USER_LOAN] = loan;
    params[USER_ORGANIZATIONID] = organizationid;
    params[USER_ADDRESS] = address;
    
    [ProgressHUD show:nil Interaction:NO];
    [APIManager apiPostURLJson:API_SIGNUP_PO uploadParam:params withCompletion:^(NSDictionary *response) {
        [USERINS saveUser:response[@"data"]];
        [self gotoMainScreen];
        [ProgressHUD dismiss];
    } failure:^(NSError *error) {
        [ProgressHUD showError:[error description]];
    }];
}


- (void)gotoMainScreen{
    SWRevealViewController *sWRevealViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:sWRevealViewController animated:YES completion:nil];
}

#pragma RKDropDownTextField Delegate
-(void)dropDownTextField:(RKDropDownTextField *)dropDownTextField didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end


#import "SplashVC.h"
#import "SignInVC.h"
#import "SignUpVC.h"

@implementation SplashVC
- (void)viewDidLoad {
    [super viewDidLoad];
    if ([FIRAuth auth].currentUser.uid) {
        FUser *user = [FUser currentUser];
        NSString *buttonTitle = [NSString stringWithFormat:@"CONTINUE AS %@",user[FUSER_FULLNAME]?user[FUSER_FULLNAME]:[FIRAuth auth].currentUser.email];
        
        [self.signupBtn setTitle:buttonTitle forState:UIControlStateNormal];
        [self.loginBtn setTitle:@"LOG OUT" forState:UIControlStateNormal];

    }else{
        [self.signupBtn setTitle:@"CREATE ACCOUNT" forState:UIControlStateNormal];
        [self.loginBtn setTitle:@"LOG IN" forState:UIControlStateNormal];
    }
} 

- (void)viewWillAppear:(BOOL)animated{

}
- (IBAction)createAccountBtn:(id)sender
{
//    [self uploadInstagram];
//    return;
    if ([FIRAuth auth].currentUser.uid) {
        [ProgressHUD show:nil Interaction:NO];        
        MainVC *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:mainVC];
        [self presentViewController:navController animated:YES completion:nil];
    }else{
        SignUpVC *sWRevealViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
        [self presentViewController:sWRevealViewController animated:NO completion:nil];

    }
}

- (IBAction)loginAccount:(id)sender
{
    if ([FIRAuth auth].currentUser.uid) {
        NSError *signOutError;
        BOOL status = [[FIRAuth auth] signOut:&signOutError];
        if (!status) {
            NSLog(@"Error signing out: %@", signOutError);
            return;
        }else{
            [self.signupBtn setTitle:@"CREATE ACCOUNT" forState:UIControlStateNormal];
            [self.loginBtn setTitle:@"LOG IN" forState:UIControlStateNormal];
        }
    }else{
        SignInVC *sWRevealViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInVC"];
        [self presentViewController:sWRevealViewController animated:NO completion:nil];
    }
}
- (void) defaultShareButtonAction
{
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    [composer setText:@"Test share"];
    [composer setImage:[UIImage imageNamed:@"marker"]];
    
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
            NSLog(@"Sending Tweet!");
        }
    }];
}
UIDocumentInteractionController *dic;

-(void)uploadInstagram{
    CGRect rect = CGRectMake(0 ,0 , 0, 0);
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIGraphicsEndImageContext();
    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/test.igo"];
    
    NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];
    dic.UTI = @"com.instagram.photo";
    dic = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
    dic=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
    [dic presentOpenInMenuFromRect: rect    inView: self.view animated: YES ];
}



- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
@end

//
//  MyfavoritesVC.m
//  ProjectH
//
//  Created by My Star on 10/21/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "MyfavoritesVC.h"
#import "HomeListCell.h"
#import "HomeDetailVC.h"
@interface MyfavoritesVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation MyfavoritesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];    
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
}
- (void) initialize
{
    GLOBALINS.mayType = MAPTYPE_MYFAVORITES;
}

#pragma mark - BUTTON ACTIONS

#pragma mark - COLLOECTION VIEW DATASOURCE
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;//GLOBALINS.homeModels.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"MyfavoriteCell";
    HomeListCell *cell = (HomeListCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    //    HomeListCell *recipeModel = GLOBALINS.recipeModels[indexPath.row];
    ////    NSLog(@"recipe Image %@",recipeModel.image);
    //    [cell.recipImg sd_setImageWithURL:[NSURL URLWithString:recipeModel.image] placeholderImage:[UIImage imageNamed:@"appicon"]];
    //    cell.recipeNameL.text = recipeModel.label;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width, height;
    width = (SCREEN_WIDTH-10)/2.0; height = width*0.667;
    return CGSizeMake(width, height);
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}
#pragma mark - COLLOECTION VIEW DELEGATE
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //    GLOBALINS.selectedRecipeModel = GLOBALINS.recipeModels[indexPath.row];
    GLOBALINS.mayType = MAPTYPE_DETAIL;
    HomeDetailVC *recipeDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [GLOBALINS.mainVC.navigationController pushViewController:recipeDetailVC animated:YES];
}

@end

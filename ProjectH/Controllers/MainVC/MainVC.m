
#import "MainVC.h"
#import "HomeMapVC.h"
@interface MainVC (){
    NSArray *pageVcA;
}

@end

@implementation MainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setReealViewGesture];    
    [self initialize];
}

-(void)setReealViewGesture{
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}
-(void)viewWillAppear:(BOOL)animated{
    
    
}
- (void) initialize
{
  //-----------------------------------------------------------------------------------------
    self.navigationItem.title = @"LISTINGS";
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = COLOR_NAVIGATIONBAR;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};

  //-----------------------------------------------------------------------------------------
    GLOBALINS.naviHeight = self.navigationController.navigationBar.frame.size.height;
    NSLog(@"%fu",GLOBALINS.naviHeight);
    NSLog(@"UserID == >%@",GLOBALINS.userId);
    NSLog(@"Token == >%@",GLOBALINS.token);
    [NotificationCenter addObserver:self selector:@selector(changeVC:) name:CHANGEVC];
    [self setContainV];
    [ProgressHUD show:nil Interaction:NO];
    [USERINS getUserFullInfoWithCompletion:^(NSDictionary *response) {
        NSError *error;
        NSDictionary *dic = response[@"data"];
        CurrentUser *user = [USERINS initWithDictionary:dic error:&error];
        NSLog(@"%@",user.email);
        [self getUserProgress];
        
    } failure:^(NSError *error) {
        [ProgressHUD showError:error.description];
    }];    
}

#pragma mark - GET DATA from SERVER
-(void)getUserProgress{
    [USERINS getUserProgressWithCompletion:^(NSDictionary *response) {
        [self getMilestoneList];
    } failure:^(NSError *error) {
        [ProgressHUD showError:error.description];
    }];
}
-(void)getMilestoneList{
    [APIManager apiGetURL:API_MILESTONES parameter:nil withCompletion:^(NSDictionary *response) {
        [self parseMilestonsWithDictionary:response];
        [ProgressHUD dismiss];
        [NotificationCenter post:CHANGEVC object:GOHOMELISTVC];
    } failure:^(NSError *error) {
        [ProgressHUD showError:error.description];
    }];
}
-(void)parseMilestonsWithDictionary :(NSDictionary *)response{
    NSArray *array = response[@"data"];
    NSError *error;
    GLOBALINS.milestones = [[NSMutableArray alloc] init];
    for (int i = 0; i<array.count; i++) {
        MilestoneModel *milstone = [[MilestoneModel alloc] initWithDictionary:array[i] error:&error];
        [GLOBALINS.milestones addObject:milstone];
    }
}
#pragma mark - BUTTON ACTION
- (IBAction)onClickMenu:(id)sender {
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
}

-(void)goMapView{
    HomeMapVC *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeMapVC"];
    [GLOBALINS.mainVC.navigationController pushViewController:mapVC animated:YES];
}
#pragma mark - SET UIS
-(void)setContainV
{
    GLOBALINS.mainVC = self;
    GLOBALINS.pageContainer = self.containerV;
    [self getCurrentStatePageVCAr];
}
- (NSArray*) getCurrentStatePageVCAr
{
    pageVcA = [[NSArray alloc]init];
    HomeListVC *homeListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeListVC"];
    MyfavoritesVC *myfavoritesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyfavoritesVC"];
    MileStoneVC *mileStoneVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MileStoneVC"];
    ProgressVC *progressVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgressVC"];
    AboutVC *aboutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutVC"];
    ContactVC *contactVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactVC"];
    SupportVC *supportVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportVC"];
    pageVcA = [NSArray arrayWithObjects:homeListVC,myfavoritesVC,mileStoneVC,progressVC,aboutVC,contactVC,supportVC,nil];
    return pageVcA;
}




- (void)switchPageViewController :(UIViewController*)buffer
{
    [GLOBALINS switchVC:buffer];
}
- (UIViewController *) getSelectedPageViewController:(NSInteger)tag
{
    UIViewController *selectedPageViewController = [[UIViewController alloc]init];
    selectedPageViewController = [self getCurrentStatePageVCAr][tag];
    return selectedPageViewController;
}

-(void) changeVC:(id)obj
{
    NSInteger index = 0;
    self.navigationItem.rightBarButtonItem = nil;
    GLOBALINS.mayType = MAPTYPE_ALLHOMES;
    if ([[obj object] isEqualToString:GOHOMELISTVC]) {
        index = 0;
        self.navigationItem.title = @"LISTINGS";
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"mapicon"]
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(goMapView)];
        GLOBALINS.mayType = MAPTYPE_ALLHOMES;
    }else if ([[obj object] isEqualToString:GOMYFAVORITESVC]) {
        index = 1;
        self.navigationItem.title = @"MY FAVORITES";
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"mapicon"]
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(goMapView)];
        GLOBALINS.mayType = MAPTYPE_MYFAVORITES;
    }else if ([[obj object] isEqualToString:GOMILESTONEVC]) {
        index = 2;
        self.navigationItem.title = @"MILESTONES";
        
    }else if ([[obj object] isEqualToString:GOPROGRESSVC]){
        index = 3;
        self.navigationItem.title = @"MY PROGRESS";
        
    }else if([[obj object] isEqualToString:GOABOUTVC]){
        index = 4;
        self.navigationItem.title = @"ABOUT PROJECT H";
    }else if([[obj object] isEqualToString:GOCONTACTVC]){
        index = 5;
        self.navigationItem.title = @"CONTACT YOUR AGENT";
    }else if([[obj object] isEqualToString:GOSUPPORTVC]){
        index = 6;
        self.navigationItem.title = @"REQUEST SUPPORT";
    }    
    [GLOBALINS switchVC:[self getSelectedPageViewController:index]];
}

- (UIImage *)imageFromView:(UIView *)view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

@end

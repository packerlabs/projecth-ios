

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
@interface SliderVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *userNameL;
@property (weak, nonatomic) IBOutlet UILabel *userEmailL;
@property (weak, nonatomic) IBOutlet UIImageView *userPhotoImg;
@property (weak, nonatomic) IBOutlet UILabel *organizationIdL;

@end

//
//  EditProfileVC.m
//  RT66 Passport
//
//  Created by My Star on 8/24/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "EditProfileVC.h"

@interface EditProfileVC (){
    UIDatePicker *datePicker;
    
}

@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.nameF becomeFirstResponder];
    [self initialize];
}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}


-(void)setUIs
{
    self.navigationItem.title = @"Edit Profile";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                             style:UIBarButtonItemStylePlain target:self action:@selector(actionBack)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(save)];    
    [_userImg setNeedsLayout];
    [_userImg layoutIfNeeded];
    [self.flagImg setNeedsLayout];
    [self.flagImg layoutIfNeeded];
    
    self.userImg.layer.cornerRadius = self.userImg.bounds.size.width/2;
    [[self.userImg layer] setBorderWidth: 2.0f];
    self.userImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.userImg.layer.masksToBounds = YES;
    if (GLOBALINS.userImg) {
        self.userImg.image = GLOBALINS.userImg;
    }
    
    self.flagImg.layer.cornerRadius = self.flagImg.bounds.size.width/2;
    self.flagImg.layer.masksToBounds = YES;
    self.flagImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    [[self.flagImg layer] setBorderWidth: 1.0f];
    
    self.salaryF.keyboardType = UIKeyboardTypeNumberPad;
    self.emailF.keyboardType = UIKeyboardTypeEmailAddress;
    
    [self setDatePickerView];
//    [self.flagImg sd_setImageWithURL:[NSURL URLWithString:flagUrl] placeholderImage:[UIImage imageNamed:@"planet"]];


    self.nameF.text = USERINS.fullName;
    self.emailF.text = USERINS.email;
    self.phoneF.text = USERINS.phone;
    self.birthF.text = USERINS.birthDate;
    self.contactF.text = USERINS.preferenceOfContact;
    self.allowTextF.text = [USERINS.allowsText isEqualToString:@"true"] ? @"Yes" : @"No" ;
    self.educationF.text = USERINS.education;
    self.salaryF.text = USERINS.annualSalary;
    self.bankerF.text = USERINS.banker;
    self.loanTypeF.text = USERINS.typeOfLoan;
    self.employerF.text = USERINS.employer;
    self.addressF.text = USERINS.address;
    
    self.allowTextF.delegate = self;
    self.allowTextF.values = ARRAY_ALLOWTEXT;
    self.allowTextF.tag = 0;
    
    self.educationF.delegate = self;
    self.educationF.values = ARRAY_EDUCATION;
    self.educationF.tag = 1;
    
    self.contactF.delegate = self;
    self.contactF.values = ARRAY_CONTACT;
    self.contactF.tag = 2;
}
-(void)initialize
{

}
#pragma mark - BUTTON ACTION
- (void)save{
    NSString *name, *email, *phone, *birthDate, *contact, *allowText, *education, *salary, *banker, *loan, *employer, *password, *address;
    name= self.nameF.text;
    email = self.emailF.text;
    phone = self.phoneF.text;
    birthDate = self.birthF.text;
    contact = self.contactF.text;
    allowText = self.allowTextF.text;
    education = self.educationF.text;
    salary = self.salaryF.text;
    banker = self.bankerF.text;
    loan = self.loanTypeF.text;
    employer = self.employerF.text;
    password = GLOBALINS.password;
    address = self.addressF.text;
    if (name.length<2) {
        [Global confirmMessage :self   :@"Error" :@"Please input valid name."];
        return;
    }
    if (![Global validEmail:email]) {
        [ProgressHUD showError:@"Please input valid email."];
        return;
    }
    if (phone.length<2) {
        [Global confirmMessage :self   :@"Error" :@"Please input valid phone number."];
        return;
    }
    if (birthDate.length<2) {
        [Global confirmMessage :self   :@"Error" :@"Please input valid birth date."];
        return;
    }
    if (contact.length<2) {
        [Global confirmMessage :self   :@"Error" :@"Please input valid contact way."];
        return;
    }
    if (allowText.length<2) {
        [Global confirmMessage :self   :@"Error" :@"Please input valid contact way."];
        return;
    }
    if (allowText.length<2) {
        [Global confirmMessage :self   :@"Error" :@"Please input valid contact way."];
        return;
    }
    if (allowText.length<2) {
        [Global confirmMessage :self   :@"Error" :@"Please input valid contact way."];
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[USER_FULLNAME] = name;
    params[USER_EMAIL] = email;
    params[USER_PASSWORD] = GLOBALINS.password;
    params[USER_PHONE] = phone;
    params[USER_PREFERENCEOFCONTACT] = contact;
    params[USER_ALLOWTEXT] = allowText;
    params[USER_EDUCATION] = education;
    params[USER_BIRTHDATE] = birthDate;
    params[USER_EMPLOYER] = employer;
    params[USER_SALARY] = @([salary doubleValue]);
    params[USER_BANKER] = banker;
    params[USER_LOAN] = loan;
//    params[USER_ADDRESS] = address;
    [ProgressHUD show:nil Interaction:NO];
    NSString *url = [NSString stringWithFormat:@"%@/%@",API_EDITEPROFILE_PU,GLOBALINS.userId];
    [APIManager apiPutURL:url uploadParam:(NSDictionary *)params withCompletion:^(NSDictionary *response) {
        [ProgressHUD showSuccess:response[@"message"]];
        
    } failure:^(NSError *error) {
        [ProgressHUD showError:error.description];
        NSLog(@"%@",error);
    }];
}
- (IBAction)birthBtn:(id)sender {
}


#pragma mark - CountriesDelegate
- (void)didSelectCountry:(NSString *)country CountryCode:(NSString *)countryCode
{

}
- (void)actionBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)openPhotoChangeV:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Open camera" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { PresentPhotoCamera(self, YES); }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Photo library" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) { PresentPhotoLibrary(self, YES); }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:action1]; [alert addAction:action2]; [alert addAction:action3];
    [self presentViewController:alert animated:YES completion:nil];

}



#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    self.userImg.image = image;
    GLOBALINS.userImg = image;
    //-----------------------------------------------------------------------------------
    UIImage *imagePicture = [Image square:image size:140];
    //-----------------------------------------------------------------------------------
    NSData *dataPicture = UIImageJPEGRepresentation(imagePicture, 0.6);
    [UserDefaults setObject:dataPicture forKey:USER_IMAGE];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)showUserPicture:(NSString *)url{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.userImg sd_setImageWithURL:[NSURL URLWithString:url]];
    });   
}
#pragma mark - DATE PICKER
- (void)setDatePickerView
{
    datePicker=[[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDate;
    [self.birthF setInputView:datePicker];
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [self.birthF setInputAccessoryView:toolBar];
    
}

-(void)ShowSelectedDate
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    self.birthF.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [self.birthF resignFirstResponder];

}
#pragma RKDropDownTextField Delegate
-(void)dropDownTextField:(RKDropDownTextField *)dropDownTextField didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
@end

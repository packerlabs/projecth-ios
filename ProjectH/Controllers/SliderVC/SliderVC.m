

#import "SliderVC.h"
//#import "SplashVC.h"
@interface SliderVC (){
    
}

@end

@implementation SliderVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
//    FUser *user = [FUser currentUser];
//    if (!user[FUSER_FULLNAME]) {
//        return;
//    }
//    self.userPhotoImg.layer.cornerRadius = self.userPhotoImg.bounds.size.width/2;
//    [[self.userPhotoImg layer] setBorderWidth: 2.0f];
//    self.userPhotoImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
//    self.userPhotoImg.layer.masksToBounds = YES;
}
- (void)viewWillAppear:(BOOL)animated{
    self.userNameL.text = USERINS.fullName;
    self.userEmailL.text = USERINS.email;
    self.organizationIdL.text = USERINS.organizationID;
    
    [self.userPhotoImg setNeedsLayout];
    [self.userPhotoImg layoutIfNeeded];
    self.userPhotoImg.layer.cornerRadius = self.userPhotoImg.bounds.size.width/2;
    [[self.userPhotoImg layer] setBorderWidth: 2.0f];
    self.userPhotoImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.userPhotoImg.layer.masksToBounds = YES;
    if (GLOBALINS.userImg) {
        self.userPhotoImg.image = GLOBALINS.userImg;
    }
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

- (IBAction)editprofilevcBtn:(id)sender
{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
    EditProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    [Global goPushFromNVC:GLOBALINS.mainVC to:vc];
}
- (IBAction)homelistvcBtn:(id)sender
{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
    [NotificationCenter post:CHANGEVC object:GOHOMELISTVC];
}
- (IBAction)myfavoritesvcBtn:(id)sender
{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
    [NotificationCenter post:CHANGEVC object:GOMYFAVORITESVC];
}
- (IBAction)milestonevcBtn:(id)sender
{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
    [NotificationCenter post:CHANGEVC object:GOMILESTONEVC];
}
- (IBAction)progressvcBtn:(id)sender {
    [NotificationCenter post:CHANGEVC object:GOPROGRESSVC];
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
}
- (IBAction)aboutvcBtn:(id)sender {
    [NotificationCenter post:CHANGEVC object:GOABOUTVC];
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
}
- (IBAction)contactvcBtn:(id)sender {
    [NotificationCenter post:CHANGEVC object:GOCONTACTVC];
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
}
- (IBAction)supportvcBtn:(id)sender {
    [NotificationCenter post:CHANGEVC object:GOSUPPORTVC];
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggle:sender];
}


- (IBAction)logoutBtn:(id)sender
{
    [UserDefaults removeObjectForKey:@"CurrentUser"];
    SignInVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInVC"];
    [Global gotoPresentFromVC:GLOBALINS.mainVC to:vc];
//    NSError *signOutError;
//    BOOL status = [[FIRAuth auth] signOut:&signOutError];
//    if (!status) {
//        NSLog(@"Error signing out: %@", signOutError);
//        return;
//    }else{
//        SplashVC *splashVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SplashVC"];
//        [self presentViewController:splashVC animated:YES completion:nil];
//    }
    
}
@end

//
//  EditProfileVC.h
//  RT66 Passport
//
//  Created by My Star on 8/24/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface EditProfileVC : UIViewController<UIImagePickerControllerDelegate,RKDropDownTextFieldDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UIImageView *flagImg;
@property (weak, nonatomic) IBOutlet UITextField *nameF;
@property (weak, nonatomic) IBOutlet UILabel *countryNameL;

@property (weak, nonatomic) IBOutlet UITextField *emailF;
@property (weak, nonatomic) IBOutlet UITextField *phoneF;

@property (weak, nonatomic) IBOutlet UITextField *birthF;
@property (weak, nonatomic) IBOutlet UITextField *salaryF;
@property (weak, nonatomic) IBOutlet UITextField *bankerF;
@property (weak, nonatomic) IBOutlet UITextField *loanTypeF;
@property (weak, nonatomic) IBOutlet UITextField *employerF;
@property (weak, nonatomic) IBOutlet UITextField *addressF;


@property (weak, nonatomic) IBOutlet RKDropDownTextField *allowTextF;
@property (weak, nonatomic) IBOutlet RKDropDownTextField *educationF;
@property (weak, nonatomic) IBOutlet RKDropDownTextField *contactF;


@end

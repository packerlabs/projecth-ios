//
//  SamplePopupViewController.m
//  CWPopupDemo
//
//  Created by Cezary Wojcik on 8/21/13.
//  Copyright (c) 2013 Cezary Wojcik. All rights reserved.
//

#import "PopStepDetailView.h"
#import <STPopup/STPopup.h>
@interface PopStepDetailView ()
{
    MyProgressModel *milestoneModel;
    NSString *milestoneName;
    NSInteger stepIndex;
}
@end

@implementation PopStepDetailView

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialize];
    [self setUIs];
    
}
- (void) initialize
{
    milestoneModel = GLOBALINS.progressModel;
    milestoneName = milestoneModel.displayTitle;
    stepIndex = GLOBALINS.selectedStepIndex;
   
}
-(void)setUIs{
    self.contentSizeInPopup = CGSizeMake(300, 300);
    self.landscapeContentSizeInPopup = CGSizeMake(400, 200);
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = COLOR_NAVIGATIONBAR;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    [self setStepInfoUI];
}


-(void)setStepInfoUI{
    StepModel *stepModel = GLOBALINS.steps[stepIndex];
    NSString *title = [NSString stringWithFormat:@"%@>Step 1:%@",milestoneName, stepModel.displayTitle];
    self.title = title;
    
    self.stepNameL.text = stepModel.displayTitle;
    self.descriptionL.text = stepModel.description;
    if ([milestoneModel.stepsCompleted containsObject:stepModel._id]) {
        [self setCompleteBtnUI:YES];
    }else{
        [self setCompleteBtnUI:NO];
    }
    [self setPreNextBtnUIs];
}
-(void)setCompleteBtnUI:(BOOL)isCompleted{
    if (isCompleted) {
        [self.markBtn setHidden:YES];
        [self.completedContainV setHidden:NO];
    }else{
        [self.markBtn setHidden:NO];
        [self.completedContainV setHidden:YES];
    }
    
}
-(void)setPreNextBtnUIs{
    if (stepIndex == 0) {
        [self.prestepBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }else{
        [self.prestepBtn setTitleColor:COLOR_NAVIGATIONBAR forState:UIControlStateNormal];
    }
    if (stepIndex == GLOBALINS.steps.count -1) {
        [self.nextstepBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }else{
        [self.nextstepBtn setTitleColor:COLOR_NAVIGATIONBAR forState:UIControlStateNormal];
    }
}
- (IBAction)contactBtn:(UIButton *)sender{
    
}
- (IBAction)markBtn:(UIButton *)sender{
    StepModel *stepModel = GLOBALINS.steps[stepIndex];
    [ProgressHUD show:nil Interaction:NO];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    params[@"stepId"] = stepModel._id;

    [APIManager apiPostURLJson:API_MARKSTEPS_PO uploadParam:(NSDictionary *)params withCompletion:^(NSDictionary *response) {

        [ProgressHUD showSuccess:response[@"message"]];
        [self setCompleteBtnUI:YES];
        [USERINS getUserProgressWithCompletion:^(NSDictionary *response) {
            [NotificationCenter post:NOTIFI_COMPLICATED];
        } failure:^(NSError *error) {
            
        }];
    } failure:^(NSError *error) {
        [ProgressHUD showError:error.description];
    }];
}
- (IBAction)prestepBtn:(UIButton *)sender{
    if (stepIndex == 0) {
        return;
    }
    stepIndex--;
    [self setStepInfoUI];
}
- (IBAction)nextstepBtn:(UIButton *)sender{
    if (stepIndex == GLOBALINS.steps.count -1) {
        return;
    }
    stepIndex++;
    [self setStepInfoUI];
}
- (IBAction)completedBtn:(UIButton *)sender{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end

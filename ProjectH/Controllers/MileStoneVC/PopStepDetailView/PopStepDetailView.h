

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface PopStepDetailView : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *stepNameL;
@property (weak, nonatomic) IBOutlet UILabel *descriptionL;
@property (weak, nonatomic) IBOutlet UIButton *contactBtn;
@property (weak, nonatomic) IBOutlet UIButton *markBtn;
@property (weak, nonatomic) IBOutlet UIButton *prestepBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextstepBtn;

@property (weak, nonatomic) IBOutlet UIView *completedContainV;
@property (weak, nonatomic) IBOutlet UIButton *completedBtn;
@end

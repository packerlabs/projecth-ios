//
//  MilestoneCell.h
//  ProjectH
//
//  Created by My Star on 10/13/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MilestoneCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerL;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *desL;

@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@end

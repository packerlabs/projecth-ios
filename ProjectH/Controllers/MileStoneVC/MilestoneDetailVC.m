
#import "MilestoneDetailVC.h"
#import "StepCell.h"
#import "PopStepDetailView.h"
@interface MilestoneDetailVC ()<UITableViewDelegate,UITableViewDataSource>
{
    MyProgressModel *myProgressModel;
    NSString *milestoneName;
    NSInteger index;
}

@end

@implementation MilestoneDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    [self setUIs];
}
-(void)viewWillAppear:(BOOL)animated{

}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    self.navigationItem.title = [NSString stringWithFormat:@"MILESTONE %lu",index + 1];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnBackMain"]
                                                                             style:UIBarButtonItemStylePlain target:self action:@selector(actionBack)];
 
    
}

- (void) initialize
{
    [NotificationCenter addObserver:self selector:@selector(reloadTableview) name:NOTIFI_COMPLICATED];
    index = [GLOBALINS.myMilestones indexOfObject:GLOBALINS.progressModel];
    myProgressModel = GLOBALINS.progressModel;
    milestoneName = myProgressModel.displayTitle;
    NSLog(@"Detail Model ==> %@",myProgressModel);
    [self getStepList];
}
- (void)getStepList{
    [ProgressHUD show:nil Interaction:NO];
    NSString *url = [NSString stringWithFormat:@"%@/%@",API_STEPLIST_G,myProgressModel.milestoneID];
    [APIManager apiGetURL:url parameter:nil withCompletion:^(NSDictionary *response) {
        [self parseStepList:response];
        [self.tableview reloadData];
        [ProgressHUD dismiss];
    } failure:^(NSError *error) {
        [ProgressHUD showError:error.description];
    }];
}
-(void)parseStepList:(NSDictionary *)response{
    NSArray *array = response[@"data"];
    NSError *error;
    GLOBALINS.steps = [NSMutableArray new];
    for (int i = 0; i< array.count ; i++) {
        StepModel *stepModel = [[StepModel alloc] initWithDictionary:array[i] error:&error];
        [GLOBALINS.steps addObject:stepModel];
    }
}
-(void)reloadTableview{
    NSString *milestoneId = myProgressModel.milestoneID;
    for (int i = 0; i< GLOBALINS.myMilestones.count; i++) {
        MyProgressModel *milestoneModel = GLOBALINS.myMilestones[i];
        if ([milestoneModel.milestoneID isEqualToString:milestoneId]) {
            myProgressModel = GLOBALINS.myMilestones[i];
            break;
        }
    }
//    myProgressModel = GLOBALINS.myMilestones[index];
    [self getStepList];
    [self.tableview reloadData];
}


#pragma mark - gesture recognizer delegate functions

// so that tapping popup view doesnt dismiss it
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return touch.view == self.view;
}
#pragma mark - BUTTON ACTIONS
- (void)actionBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)checkBtn:(UIButton *)sender{
    StepModel *stepModel = GLOBALINS.steps[sender.tag];
    if (stepModel.completed) {
        
    }else{
        sender.selected = !sender.isSelected;
    }
    
}
- (IBAction)viewMoreBtn:(id)sender {
    [self showDetailStepPopuVCWithStepIndex:0];
}

-(void)showDetailStepPopuVCWithStepIndex :(NSInteger)index{    
    GLOBALINS.selectedStepIndex = index;
    PopStepDetailView *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopStepDetailView"];
    vc.contentSizeInPopup = CGSizeMake(300, 300);
    vc.landscapeContentSizeInPopup = CGSizeMake(400, 200);
    [Global popupVCwithFrom:self to:vc];
}
-(void)stepNameBtn:(UIButton *)sender{
    [self showDetailStepPopuVCWithStepIndex:sender.tag];
}
#pragma mark - TABLEVIEW DELEGATE
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return GLOBALINS.steps.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StepCell *cell = (StepCell *)[tableView dequeueReusableCellWithIdentifier:@"StepCell" forIndexPath:indexPath];
    StepModel *stepModel = GLOBALINS.steps[indexPath.row];
    cell.nameL.text = stepModel.displayTitle;
    if ([myProgressModel.stepsCompleted containsObject:stepModel._id]) {
        cell.checkBtn.selected = YES;
    }else{
        cell.checkBtn.selected = NO;
    }

    cell.checkBtn.tag = indexPath.row;
    cell.nameBtn.tag = indexPath.row;
    [cell.checkBtn addTarget:self action:@selector(checkBtn:) forControlEvents: UIControlEventTouchUpInside];
    [cell.nameBtn addTarget:self action:@selector(stepNameBtn:) forControlEvents: UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self showDetailStepPopuVCWithStepIndex:indexPath.row];
}
@end

//
//  BufferVC.h
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface HomeMapVC : UIViewController
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *mapnormalBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapbirdBtn;

@end

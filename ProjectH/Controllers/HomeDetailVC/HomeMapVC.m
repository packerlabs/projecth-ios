//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "HomeMapVC.h"

@interface HomeMapVC ()<GMSMapViewDelegate>{
    float zoomValue;
    NSArray *homeLocations;
}

@end

@implementation HomeMapVC

- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{
    
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    self.mapnormalBtn.selected = YES;
    self.mapbirdBtn.selected = NO;
    [self mapSet];
}
- (void) initialize
{
    switch (GLOBALINS.mayType) {
        case MAPTYPE_ALLHOMES:
            zoomValue = MAPZOOM_ALLHOMES;

            break;
        case MAPTYPE_MYFAVORITES:
            zoomValue = MAPZOOM_MYFAVORITES;
            
            break;
        case MAPTYPE_DETAIL:
            zoomValue = MAPZOOM_DETAIL;
            
            break;
        default:
            break;
    }
   
}
- (void)mapSet
{
    float latitu = 33.885363;
    float longitu = -112.154194;
    float step = 0.001;
    
    self.mapView.delegate = self;
    self.mapView.myLocationEnabled=YES;
    self.mapView.settings.rotateGestures = NO;
    self.mapView.settings.tiltGestures = NO;
    GMSCameraPosition *mylocation = [GMSCameraPosition cameraWithLatitude:latitu
                                                                longitude:longitu
                                                                     zoom:zoomValue];
    
    [self.mapView setCamera:mylocation];

    if (GLOBALINS.mayType == MAPTYPE_ALLHOMES) {
        for (int i = 0; i<10; i++) {
            GMSMarker *marker = [GMSMarker new];
            latitu += step;
            longitu +=step;
            marker.icon = [UIImage imageNamed:@"mapIcon"];
            marker.position = CLLocationCoordinate2DMake(latitu, longitu);
            marker.map = self.mapView;
        }
    }else if(GLOBALINS.mayType == MAPTYPE_MYFAVORITES){
        for (int i = 0; i<3; i++) {
            GMSMarker *marker = [GMSMarker new];
            latitu += step;
            longitu +=step;
            marker.icon = [UIImage imageNamed:@"mapIcon"];
            marker.position = CLLocationCoordinate2DMake(latitu, longitu);
            marker.map = self.mapView;
        }
    }else{
        GMSMarker *marker = [GMSMarker new];
        marker.position = CLLocationCoordinate2DMake(latitu, longitu);
        marker.icon = [UIImage imageNamed:@"mapIcon_black"];
        marker.map = self.mapView;
    }
}
#pragma mark - BUTTON ACTIONS

- (IBAction)mapnormalBtn:(UIButton *)sender {
    if (sender.isSelected) {
        return;
    }
    sender.selected = !sender.isSelected;
    self.mapbirdBtn.selected = NO;
    self.mapView.mapType = kGMSTypeNormal;
}
- (IBAction)mapbirdBtn:(UIButton *)sender {
    if (sender.isSelected) {
        return;
    }
    self.mapView.mapType = kGMSTypeSatellite;
    self.mapnormalBtn.selected = NO;
    sender.selected = !sender.isSelected;

}
- (IBAction)navigateBtn:(id)sender {
    NSString *address = @"28614 N 21st Ave,Phoenix, AZ 85085";
    NSString *str1 = [address stringByReplacingOccurrencesOfString:@" " withString:@",+"];
    NSString *str2 = [str1 stringByReplacingOccurrencesOfString:@",+,+" withString:@",+"];
    NSString *str3 = [str2 stringByReplacingOccurrencesOfString:@",," withString:@","];
    NSString *addressString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",str3];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:addressString]];
}
@end

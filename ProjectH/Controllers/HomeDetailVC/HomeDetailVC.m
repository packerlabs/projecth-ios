//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "HomeDetailVC.h"
#import "HomeMapVC.h"
#import "CalculateVC.h"
#import "AgentsVC.h"
#import "QuestionVC.h"
#import "SharingVC.h"
@interface HomeDetailVC ()<GMSMapViewDelegate>{

}

@end

@implementation HomeDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{
    
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}
- (void) initialize
{
    [NotificationCenter addObserver:self selector:@selector(imageCount:) name:NOTI_SLIDEIMAGEINDEX];
    [self mapSet];
}
-(void)setUIs{
    [self showContactContainV];
    
    UIBarButtonItem *buttonRight1 = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"unlike"] style:UIBarButtonItemStylePlain
                                                                    target:self action:@selector(likeAction:)];
    UIBarButtonItem *buttonRight2 = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"share"] style:UIBarButtonItemStylePlain
                                                                    target:self action:@selector(shareAction:)];
    self.navigationItem.rightBarButtonItems = @[buttonRight1, buttonRight2];
    [Global roundCornerSet:self.photoCountL];
    self.photoCountL.text = [NSString stringWithFormat:@"1/4"];
}
-(void)imageCount:(id)obj{
    NSInteger index = [[obj object] integerValue];
    self.photoCountL.text = [NSString stringWithFormat:@"%lu/4",index];
}

#pragma mark - MAP SET/ DELEGATE
- (void)mapSet
{
    self.mapView.delegate = self;
    self.mapView.myLocationEnabled=YES;
    self.mapView.settings.rotateGestures = NO;
    self.mapView.settings.tiltGestures = NO;//33.885363, -112.154194
    GMSCameraPosition *mylocation = [GMSCameraPosition cameraWithLatitude:33.885363
                                                                longitude:-112.154194
                                                                     zoom:18.0];
    GMSMarker *marker = [GMSMarker new];
    float latitu = 33.885363;
    float longitu = -112.154194;
    marker.position = CLLocationCoordinate2DMake(latitu, longitu);
    marker.icon = [UIImage imageNamed:@"mapIcon"];
    marker.map = self.mapView;
    [self.mapView setCamera:mylocation];   

}
#pragma mark - BUTTON ACTIONS
- (IBAction)goMapview:(id)sender {
    HomeMapVC *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeMapVC"];
    [GLOBALINS.mainVC.navigationController pushViewController:mapVC animated:YES];  
}
- (IBAction)mortgageCalculBtn:(id)sender {
    CalculateVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CalculateVC"];
    vc.contentSizeInPopup = CGSizeMake(320, 450);
    vc.landscapeContentSizeInPopup = CGSizeMake(320, 480);
    [Global popupVCwithFrom:self to:vc];
}
- (IBAction)getQualifyBtn:(id)sender {
    QuestionVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionVC"];
    [Global gotoPresentFromVC:self to:vc];
}
- (IBAction)contactAgentBtn:(id)sender {
    AgentsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AgentsVC"];
    vc.contentSizeInPopup = CGSizeMake(320, 480);
    vc.landscapeContentSizeInPopup = CGSizeMake(320, 480);
    [Global popupVCwithFrom:self to:vc];
    [self hideContactContainV];
}

-(void)likeAction:(UIBarButtonItem*)sender{
//    sender.image = [UIImage imageNamed:@"like"];
}
-(void)shareAction:(UIBarButtonItem*)sender{
//    sender.image = [UIImage imageNamed:@"like"];
    SharingVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SharingVC"];
    vc.contentSizeInPopup = CGSizeMake(220, 180);
    vc.landscapeContentSizeInPopup = CGSizeMake(220, 180);
    [Global popupVCwithFrom:self to:vc];
    
//    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:vc];
//    popupController.style = STPopupStyleBottomSheet;
//    [popupController presentInViewController:self];
}
- (void)showContactContainV{
    CGFloat viewHeght = self.contactContainV.bounds.size.height;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    CGFloat h = self.view.frame.size.height;
    [UIView animateWithDuration:0.4 animations:^{
        self.contactContainV.frame = CGRectMake(0, h - viewHeght, self.contactContainV.frame.size.width, viewHeght);
    } completion:^(BOOL finished) {
        
    }];
    
}
- (void)hideContactContainV
{
    CGFloat viewHeght = self.contactContainV.bounds.size.height;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    CGFloat h = self.view.frame.size.height;
    [UIView animateWithDuration:0.4 animations:^{
        self.contactContainV.frame = CGRectMake(0, h , self.contactContainV.frame.size.width, viewHeght);
    } completion:^(BOOL finished) {
        
    }];
}
@end

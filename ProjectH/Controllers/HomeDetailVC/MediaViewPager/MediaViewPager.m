//
//  MediaViewPager.m
//  ProjectH
//
//  Created by My Star on 10/22/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "MediaViewPager.h"
#import "LPTitleView.h"
#import "VideoVC.h"
#import "ImagesVC.h"
@interface MediaViewPager ()<LPViewPagerDataSource, LPViewPagerDelegate, LPTitleViewDelegate>
@property (nonatomic, strong) VideoVC *videoVC;
@property (nonatomic, strong) ImagesVC *imagesVC;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) LPTitleView *pagingTitleView;
@end

@implementation MediaViewPager

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pagerSet];
}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
    
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    
}
- (void) initialize
{
    GLOBALINS.selectedQuestionViewIndex = 0;
}
- (void)pagerSet
{
    self.dataSource = self;
    self.delegate = self;
    // Do not auto load data
    self.manualLoadData = YES;
    self.currentIndex = 0;
    self.navigationItem.titleView = self.pagingTitleView;
    [self reloadData];
}


#pragma mark - LPViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(LPViewPagerController *)viewPager
{
    return 3;
}

- (UIViewController *)viewPager:(LPViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index
{
    if(index == 0) {
        return [self createVc1];
    }else if (index == 1) {
        return [self createVc2];
    }else{
        return [self createVc1];
    }
}
-(void)selectViewIndex{
    [self didSelectedTitleAtIndex:GLOBALINS.selectedQuestionViewIndex];
}
#pragma mark 🎈LPViewPagerDelegate
- (void)viewPager:(LPViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    self.currentIndex = index;
}

- (LPTitleView *)pagingTitleView
{
    if (!_pagingTitleView) {
        self.pagingTitleView          = [[LPTitleView alloc] init];
        self.pagingTitleView.frame    = CGRectMake(0, 0, 0, 40);
        self.pagingTitleView.font     = [UIFont systemFontOfSize:15];
        //        self.pagingTitleView.titleNormalColor = [UIColor greenColor];
        //        self.pagingTitleView.titleSelectedColor = [UIColor yellowColor];
        self.pagingTitleView.indicatorColor = [UIColor purpleColor];
        NSArray *titleArray           = @[@"Type", @"Categories"];
        CGRect ptRect                 = self.pagingTitleView.frame;
        ptRect.size.width             = [LPTitleView calcTitleWidth:titleArray withFont:self.pagingTitleView.font];
        self.pagingTitleView.frame    = ptRect;
        [self.pagingTitleView addTitles:titleArray];
        self.pagingTitleView.delegate = self;
    }
    return _pagingTitleView;
}

- (void)didSelectedTitleAtIndex:(NSUInteger)index
{
    UIPageViewControllerNavigationDirection direction;
    if (self.currentIndex == index) {
        return;
    }
    if (index > self.currentIndex) {
        direction = UIPageViewControllerNavigationDirectionForward;
    } else {
        direction = UIPageViewControllerNavigationDirectionReverse;
    }
    UIViewController *viewController = [self viewControllerAtIndex:index];
    if (viewController) {
        __weak typeof(self) weakself = self;
        [self.pageViewController setViewControllers:@[viewController] direction:direction animated:YES completion:^(BOOL finished) {
            weakself.currentIndex = index;
        }];
    }
}

- (void)setCurrentIndex:(NSInteger)index
{
    _currentIndex = index;
    [self.pagingTitleView adjustTitleViewAtIndex:index];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat contentOffsetX = scrollView.contentOffset.x;
    CGFloat screenWidth = [[UIScreen mainScreen]bounds].size.width;
    if (self.currentIndex != 0 && contentOffsetX <= screenWidth * 2) {
        contentOffsetX += screenWidth * self.currentIndex;
    }
    [self.pagingTitleView updatePageIndicatorPosition:contentOffsetX];
}

- (void)scrollEnabled:(BOOL)enabled
{
    self.scrollingLocked = !enabled;
    for (UIScrollView *view in self.pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = enabled;
            view.bounces = enabled;
        }
    }
}

#pragma mark - lazy load

- (UIViewController *)createVc1
{
    if (!self.videoVC) {
        self.videoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoVC"];
    }
    return self.videoVC;
}

- (UIViewController *)createVc2
{
    if (!self.imagesVC) {
        self.imagesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ImagesVC"];
    }
    return self.imagesVC;
}



@end

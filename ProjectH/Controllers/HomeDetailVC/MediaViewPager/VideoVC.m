//
//  VideoVC.m
//  ProjectH
//
//  Created by My Star on 10/22/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "VideoVC.h"

@interface VideoVC ()

@end

@implementation VideoVC
- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
    [NotificationCenter post:NOTI_SLIDEIMAGEINDEX object:@(1)];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{
    [self.videoV pause];
}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"houseSampleVideo" ofType:@"mp4"];
    NSURL *videoURL = [NSURL fileURLWithPath:filepath];
    self.videoV.videoUrl = videoURL; // mp4 playable
    [self.videoV play];
}
- (void) initialize
{
}
#pragma mark - BUTTON ACTIONS

@end

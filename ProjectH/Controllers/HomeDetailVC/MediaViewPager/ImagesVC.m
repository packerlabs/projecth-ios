//
//  ImagesVC.m
//  ProjectH
//
//  Created by My Star on 10/22/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "ImagesVC.h"

@interface ImagesVC (){
    ZMImageSliderView *imageSliderView;
    NSArray *imageURLs;
    NSMutableArray *iDMPhotos;
    
}

@end

@implementation ImagesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
    [NotificationCenter post:NOTI_SLIDEIMAGEINDEX object:@(2)];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    imageSliderView = [[ZMImageSliderView alloc] initWithOptions:0 imageUrls:imageURLs];
    imageSliderView.translatesAutoresizingMaskIntoConstraints = NO;
    imageSliderView.delegate = self;
    [self imageSliderViewImageSwitch:0 count:[imageURLs count] imageUrl:[imageURLs objectAtIndex:0]];    
    
    [self.view addSubview:imageSliderView];
    [self setImageSliderViewConstraints];
}
- (void) initialize
{
    imageURLs = [NSArray arrayWithObjects:
                 @"http://grandcanyon.com/wp-content/uploads/2014/03/horseshoe-bend-page-arizona.jpg",@"http://res.cloudinary.com/simpleview/image/upload/v1472257613/clients/scottsdale/Stock_Grand_Canyon_084fa36e-0a2d-4560-9dbd-bbd0d93d6eb8.jpg",@"https://content-oars.netdna-ssl.com/wp-content/uploads/2015/12/gc-dory-lees-phantom-hero.jpg",
                 nil];
    [self setIDMPhotos];
}
#pragma mark - BUTTON ACTIONS

#pragma mark - SET SLIDERVIEW CONSTRAINTS
- (void)setImageSliderViewConstraints {
    NSArray *imageSliderViewHConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[imageSliderView]-0-|"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{@"imageSliderView": imageSliderView}];

    [self.view addConstraints:imageSliderViewHConstraints];



    NSArray *imageSliderViewVConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[imageSliderView]-0-|"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{@"imageSliderView": imageSliderView}];
    [self.view addConstraints:imageSliderViewVConstraints];
}
#pragma mark - ZMImageSliderViewDelegate
- (void)imageSliderViewSingleTap:(UITapGestureRecognizer *)tap {
    [self goIDMPhotoBrowser];
//        [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageSliderViewImageSwitch:(NSInteger)index count:(NSInteger)count imageUrl:(NSString *)imageUrl {
    [NotificationCenter post:NOTI_SLIDEIMAGEINDEX object:@(index+2)];
//    self.displayLabel.text = [[NSString alloc] initWithFormat:@"%td／%td", index + 1, count];
}

-(void)setIDMPhotos{
    if (!iDMPhotos) {
        iDMPhotos = [[NSMutableArray alloc] init];
    }
    for (int i=0; i<imageURLs.count; i++) {
        IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:imageURLs[i]]];
        [iDMPhotos addObject:photo];
    }

}
-(void)goIDMPhotoBrowser{
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:iDMPhotos animatedFromView:imageSliderView];
//    browser.scaleImage = imageSliderView;
    //    browser.delegate = self;
    browser.displayCounterLabel = YES;
    browser.displayActionButton = NO;
    [self presentViewController:browser animated:YES completion:nil];
}
@end

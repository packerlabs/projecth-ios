//
//  MediaViewPager.h
//  ProjectH
//
//  Created by My Star on 10/22/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
#import <QuartzCore/QuartzCore.h>
#import "LPViewPagerController.h"
@interface MediaViewPager : LPViewPagerController

@end

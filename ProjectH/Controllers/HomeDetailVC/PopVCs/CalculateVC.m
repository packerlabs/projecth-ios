//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "CalculateVC.h"

@interface CalculateVC (){
    NSMutableArray *loanterms;
}

@end

@implementation CalculateVC

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    self.title = @"  ESTIMATED MONTHLY PAYMENT";
    self.loantermF.delegate = self;
    self.loantermF.text = loanterms[0];
    self.loantermF.values = loanterms;
    
    [Global roundCornerSet:self.getPreQualifiBtn];

}
- (void) initialize
{
    loanterms = [NSMutableArray arrayWithObjects:@"30-year fixed",@"15-year fixed",@"5/1 ARM", nil];
}
#pragma mark - BUTTON ACTIONS

- (IBAction)getQualifiBtn:(id)sender {
    QuestionVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionVC"];
    [Global gotoPresentFromVC:self to:vc];
}
#pragma RKDropDownTextField Delegate
-(void)dropDownTextField:(RKDropDownTextField *)dropDownTextField didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
@end

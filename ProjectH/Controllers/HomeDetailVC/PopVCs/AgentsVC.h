//
//  BufferVC.h
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
@interface AgentsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *callContainV;
@property (weak, nonatomic) IBOutlet UIView *emailContainV;


@property (weak, nonatomic) IBOutlet UIButton *contactBtn;
@property (weak, nonatomic) IBOutlet UIButton *financBtn;

@property (weak, nonatomic) IBOutlet UITextField *userNameF;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberF;
@property (weak, nonatomic) IBOutlet UITextField *emailF;

@property (weak, nonatomic) IBOutlet UITextView *contactInfoFV;
@end

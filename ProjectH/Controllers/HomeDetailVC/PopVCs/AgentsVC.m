//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "AgentsVC.h"

@interface AgentsVC ()<MFMailComposeViewControllerDelegate>

@end

@implementation AgentsVC

- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewWillAppear:(BOOL)animated{
    [self initialize];
    [self setUIs];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    self.title = @"CONTACT AGENT";
    self.userNameF.text = USERINS.fullName;
    self.emailF.text = USERINS.email;
    self.phoneNumberF.text = USERINS.phone;
    
    if ([[USERINS.preferenceOfContact lowercaseString] isEqualToString:USER_PHONE]) {
        [self.emailContainV setHidden:YES];
        [self.callContainV setHidden:NO];
    }else{
        [self.emailContainV setHidden:NO];
        [self.callContainV setHidden:YES];
    }
}
- (void) initialize
{
    [Global roundCornerSet:self.contactBtn];
}
#pragma mark - BUTTON ACTIONS

- (IBAction)financBtn:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}
- (IBAction)contactBtn:(id)sender {
    if ([[USERINS.preferenceOfContact lowercaseString] isEqualToString:USER_PHONE]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",@"2142365177"]]];
    }else{
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            [mailer setSubject:@"I'm homebuyer!"];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"team@packerlabs.com", nil];
            [mailer setToRecipients:toRecipients];

            
            [mailer setMessageBody:self.contactInfoFV.text isHTML:NO];
            [self presentViewController:mailer animated:YES completion:nil];
            return;
        }else{
            [ProgressHUD showError:@"Please set your email account!"];
            return;
        }
    }
}
#pragma mark - MFMailComposeViewController DelegateMethod
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:{            
            break;
        }
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end

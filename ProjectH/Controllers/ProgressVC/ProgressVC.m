//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "ProgressVC.h"

@interface ProgressVC ()

@end

@implementation ProgressVC

- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
    [self getUserProgress];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
    [self initFlatWithIndicatorProgressBar];
    [self setProgress:GLOBALINS.progress animated:YES];
}
- (void) initialize
{
}

#pragma mark - GET DATA from SERVER
-(void)getUserProgress{
    [USERINS getUserProgressWithCompletion:^(NSDictionary *response) {
        [self.collectionView reloadData];
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - BUTTON ACTIONS
- (void)goModelDetailVC:(UIButton *)sender{
    GLOBALINS.progressModel = GLOBALINS.myMilestones[sender.tag];
    MilestoneDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MilestoneDetailVC"];
    [Global goPushFromNVC:GLOBALINS.mainVC to:vc];
}
#pragma mark - YLProgressBar
- (void)initFlatWithIndicatorProgressBar
{
    self.progressBar.type                     = YLProgressBarTypeFlat;
    self.progressBar.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeProgress;
    self.progressBar.behavior                 = YLProgressBarBehaviorIndeterminate;
    self.progressBar.stripesOrientation       = YLProgressBarStripesOrientationVertical;
}
- (void)setProgress:(CGFloat)progress animated:(BOOL)animated
{
    [self.progressBar setProgress:progress animated:animated];    
}

#pragma mark - COLLECTIONVIEW DELEGATE
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return GLOBALINS.myMilestones.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"MilestoneCell";
    MilestoneCell *cell = (MilestoneCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    MyProgressModel *milestone = GLOBALINS.myMilestones[indexPath.row];
    cell.headerL.text = [NSString stringWithFormat:@"MILESTONE %lu",indexPath.row +1];
    cell.titleL.text = milestone.displayTitle;
    cell.desL.text = milestone.description;
    cell.startBtn.tag = indexPath.row;
    [cell.startBtn addTarget:self action:@selector(goModelDetailVC:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 20 )/2.0,SCREEN_HEIGHT/3.0);
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}
#pragma mark - BUTTON ACTIONS

@end

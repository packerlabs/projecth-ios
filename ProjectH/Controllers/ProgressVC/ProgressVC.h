//
//  BufferVC.h
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
#import "MilestoneCell.h"
#import "MilestoneDetailVC.h"
@class YLProgressBar;
@interface ProgressVC : UIViewController
@property (weak, nonatomic) IBOutlet YLProgressBar *progressBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

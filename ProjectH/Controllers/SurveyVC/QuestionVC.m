//
//  QuestionVC.m
//  ProjectH
//
//  Created by My Star on 10/20/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionVC.h"

@interface QuestionVC ()

@end

@implementation QuestionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [NotificationCenter addObserver:self selector:@selector(progresValue:) name:NOTI_CHANGEPROGRESS];
}

-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs
{
    self.circleV.roundedCorners = YES;
    self.circleV.progress = 0.0;
    self.circleV.thicknessRatio = 0.2;
    self.circleV.progressTintColor = HEXCOLOR(0xF66420FF);  
    
    self.userImg.layer.cornerRadius = self.userImg.bounds.size.width/2;
    [[self.userImg layer] setBorderWidth: 2.0f];
    self.userImg.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.userImg.layer.masksToBounds = YES;
    
    self.nextBtn.layer.cornerRadius = self.userImg.bounds.size.width/2;
    [[self.nextBtn layer] setBorderWidth: 2.0f];
    self.nextBtn.layer.borderColor = HEXCOLOR(0x2B6A9DFF).CGColor;
    self.nextBtn.layer.masksToBounds = YES;
}
- (void) initialize
{
}
#pragma mark - BUTTON ACTIONS

- (IBAction)backBtn:(id)sender {
    GLOBALINS.selectedQuestionViewIndex --;
    if (GLOBALINS.selectedQuestionViewIndex <0) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        return;
    }
    [NotificationCenter post:NOTI_QUESTIONCHANGE];
    
}
-(void) progresValue:(id)obj
{
    float value = [[obj object] floatValue];
    self.circleV.progress = value;
   
}
@end

//
//  BufferVC.m
//  RT66 Passport
//
//  Created by My Star on 8/27/17.
//  Copyright © 2017 My Star. All rights reserved.
//

#import "QuestionTwoVC.h"

@interface QuestionTwoVC ()

@end

@implementation QuestionTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewWillAppear:(BOOL)animated{
    [self setUIs];
    [self initialize];
}
-(void)viewDidDisappear:(BOOL)animated{}
-(void)viewWillDisappear:(BOOL)animated{}
- (void)didReceiveMemoryWarning {[super didReceiveMemoryWarning];}

-(void)setUIs{
}
- (void) initialize
{
    [NotificationCenter post:NOTI_CHANGEPROGRESS object:@(0.33)];
}
#pragma mark - BUTTON ACTIONS
- (IBAction)noBtn:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    GLOBALINS.selectedQuestionViewIndex ++;
    [NotificationCenter post:NOTI_QUESTIONCHANGE];
}
- (IBAction)yesBtn:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    GLOBALINS.selectedQuestionViewIndex ++;
    [NotificationCenter post:NOTI_QUESTIONCHANGE];
}
@end
